var dict = {
    1: "#0487cc",
    2: "#f23a3a",
    3: "#2eac6d",
    4: "#f35b25",
    5: "#00adb5",
    6: "#e4c144",
}

function randomChangeColor() {

    now = new Date;

    document.getElementById("time").innerHTML = now.getFullYear() - 2015;
    document.getElementById("age").innerHTML = now.getFullYear() - 1991;

    var number = Math.ceil(Math.random() * 6);

    var lista = document.getElementsByTagName('span')
    inputColor(lista, number)

    var lista = document.getElementsByClassName('progress-bar')
    inputBackgroundColor(lista, number)

    var lista = document.querySelectorAll("#education h3")
    inputBackgroundColor(lista, number)

    var lista = document.querySelectorAll("#experience h3")
    inputBackgroundColor(lista, number)

    var lista = document.querySelectorAll("h2")
    inputColor(lista, number)
}

function inputBackgroundColor(pLista, pNumber) {
    for (i = 0; i < pLista.length; i++) {
        pLista[i].style.backgroundColor = dict[pNumber]
    }
}

function inputColor(pLista, pNumber) {
    for (i = 0; i < pLista.length; i++) {
        pLista[i].style.color = dict[pNumber]
    }
}